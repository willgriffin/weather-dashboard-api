import WeatherLookup from '../../src/WeatherLookup'
import { expect } from 'chai'

describe('WeatherLookup.spec.js', () => {

  it('should be able to lookup weather about a known city', async() => {
    const result = await WeatherLookup.getWeather('dease_lake')
    expect(result.entries?.length).to.be.greaterThan(0)
  })

  it('should be able to lookup details about a known city', async() => {
    const result = await WeatherLookup.getCity('dease_lake')
    expect(result.name).to.equal('Dease Lake')
  })

  it('should be able to lookup a current condiditions given a known city', async() => {
    const result = await WeatherLookup.getCurrentConditions('dease_lake')
    expect(result.type).to.equal('Current Conditions')
  })

  // used to get a look at the data returned, no extra functionality testing than already exists above so skipping
  it.skip('should be able to lookup all the cities and get weather for them', async() => {
    const cities = await WeatherLookup.getCities()
    for (const city of cities) {
      city.current = await WeatherLookup.getCurrentConditions(city.slug)
      console.log(city.current)
    }
  })

})