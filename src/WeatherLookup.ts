// import * as rp from 'request-promise'
// import * as moment from 'moment'
import * as ecweather from 'ec-weather'
import * as _ from 'lodash'

interface WeatherEntryInterface {
  type: string;
  title: string;
  link: string;
  updated: string;
  published: string;
  summary: string;
  observedAt?: string;
  condition?: string;
  temperature?: string;
  pressureTendency?: string;
  visibility?: string;
  humidity?: string;
  dewpoint?: string;
  wind?: string;
  airQualityHealthIndex?: string;
  inEffect?: boolean; // for warnings
}

interface WeatherResultsInterface {
  lang: string;
  city: string;
  title: string;
  badgeUrl: string;
  updated: string;
  rights: string;
  entries: Array<WeatherEntryInterface>
  author: {
    name: string;
    uri: string;
  }
}

interface CityInterface {
  name: string;
  slug: string;
  code: string;
  lat: number;
  lng: number;
  current?: WeatherEntryInterface;
}

export default class WeatherLookup {
  
  public static async getCurrentConditions(cityName: string): Promise<WeatherEntryInterface> {
    const weather = await WeatherLookup.getWeather(cityName);
    const currentConditions = _.find(weather.entries, { 'type': 'Current Conditions' });
    return currentConditions;
  }
  
  public static async getWeather(cityName: string): Promise<WeatherResultsInterface> {
    const city = await WeatherLookup.getCity(cityName)
    const weather = await ecweather({
      lang: 'en',
      city: city.code
    })
    //console.log(_.find(weather.entries, { 'type': 'Current Conditions' }))
    return weather
  }
  
  public static async getCities(): Promise<Array<CityInterface>> {
    const cities = [
      {
        slug: 'dease_lake',
        name: 'Dease Lake',
        code: 'bc-14',
        lat: 58.4374,
        lng: -129.9994
      },
      {
        slug: 'fort_nelson',
        name: 'Fort Nelson',
        code: 'bc-83',
        lat: 58.8050,
        lng: -122.6972
      },
      {
        slug: 'terrace',
        name: 'Terrace',
        code: 'bc-80',
        lat: 54.5182,
        lng: -128.6032
      },
      {
        slug: 'prince_george',
        name: 'Prince George',
        code: 'bc-79',
        lat: 53.9171,
        lng: -122.7497
      },
      {
        slug: 'whistler',
        name: 'Whistler',
        code: 'bc-86',
        lat: 50.1163,
        lng: -122.9574
      },
      {
        slug: 'revelstoke',
        name: 'Revelstoke',
        code: 'bc-65',
        lat: 50.9981,
        lng: -118.1957
      },
      {
        slug: 'creston',
        name: 'Creston',
        code: 'bc-26',
        lat: 49.0955,
        lng: -116.5135
      },  
    ]
    return cities;
  }
  
  // making an async function so in the future when a proper lookup is put into place there's no refactoring required
  public static async getCity(citySlug: string): Promise<any> {
    const cities = await WeatherLookup.getCities()
    const result =  _.find(cities, { 'slug': citySlug });
    if (typeof result === 'undefined') {
      throw new Error(`I don't know a city named ${citySlug}`)
    }
    return result
  }
  
}