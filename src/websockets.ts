const AWS = require('aws-sdk')

const dynamoDbOptions:{
  region?: string;
  endpoint?: string;
} = {};

const ApiGatewayManagementOptions: {
  apiVersion: string;
  endpoint?: string;
} = {
  apiVersion: '2018-11-29',
}

// connect to local DB if running offline
if (process.env.IS_OFFLINE) {
  dynamoDbOptions.region = 'localhost',
  dynamoDbOptions.endpoint = 'http://localhost:8000',
  ApiGatewayManagementOptions.endpoint = 'http://localhost:3001'
}

const dynamodb = new AWS.DynamoDB.DocumentClient(dynamoDbOptions);
const { Service, apiLoader } = AWS

apiLoader.services['apigatewaymanagementapi'] = {}

const model = {
  metadata: {
    apiVersion: '2018-11-29',
    endpointPrefix: 'execute-api',
    signingName: 'execute-api',
    serviceFullName: 'AmazonApiGatewayManagementApi',
    serviceId: 'ApiGatewayManagementApi',
    protocol: 'rest-json',
    jsonVersion: '1.1',
    uid: 'apigatewaymanagementapi-2018-11-29',
    signatureVersion: 'v4'
  },
  operations: {
    PostToConnection: {
      http: {
        requestUri: '/@connections/{connectionId}',
        responseCode: 200
      },
      input: {
        type: 'structure',
        members: {
          Data: {
            type: 'blob'
          },
          ConnectionId: {
            location: 'uri',
            locationName: 'connectionId'
          }
        },
        required: ['ConnectionId', 'Data'],
        payload: 'Data'
      }
    }
  },
  paginators: {},
  shapes: {}
}

AWS.ApiGatewayManagementApi = Service.defineService('apigatewaymanagementapi', ['2018-11-29'])
Object.defineProperty(apiLoader.services['apigatewaymanagementapi'], '2018-11-29', {
  // eslint-disable-next-line
  get: function get() {
    return model
  },
  enumerable: true,
  configurable: true
})
/* END ApiGatewayManagementApi injection */

export const connect = async (event: any = {}, context: any = {}): Promise<any> => {
  const { domainName, stage, connectionId } = event.requestContext
  const connectedAt = new Date()
  await dynamodb.put({
    TableName: process.env.WEBSOCKET_CLIENTS_TABLE,
    Item: {
      domainName,
      stage,
      connectionId,
      connectedAt
    },
  }).promise()


  const client = new AWS.ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint: `https://${event.requestContext.domainName}/${event.requestContext.stage}`
  })
  await client.postToConnection({
    ConnectionId: connectionId,
    Data: 'welcome'
  })

  const response = {
    statusCode: 200,
    body: `Connected`
  }
  return response
}

export const disconnect = async (event: any = {}, context: any = {}): Promise<any> => {
  const response = {
    statusCode: 200,
    body: 'Disconnected.'
  }
  return response
}

export const updateClients = async (event: any = {}, context: any = {}): Promise<any> => {

  await socketBlastAll({herp: 'derp'})
  // const result = await dynamodb.scan({
  //   TableName: process.env.WEBSOCKET_CLIENTS_TABLE,
  // }).promise()

  // if (result.Items.length > 0) {

  // }

  // for (const connection of result.items) {

  // }

  // const client = new AWS.ApiGatewayManagementApi({
  //   apiVersion: '2018-11-29',
  //   endpoint: `https://${event.requestContext.domainName}/${event.requestContext.stage}`
  // });

  // await client
  //   .postToConnection({
  //     ConnectionId: event.requestContext.connectionId,
  //     Data: `heyo ${event.requestContext.connectionId}`
  //   })
  //   .promise();
}


export const socketBlastAll = async (data) => {
  
  const result = await dynamodb.scan({
    TableName: process.env.WEBSOCKET_CLIENTS_TABLE,
  }).promise()

  if (result.Items.length > 0) {
    //console.log(ApiGatewayManagementOptions)
    //const client = new AWS.ApiGatewayManagementApi(ApiGatewayManagementOptions);
    for (const connection of result.Items) {
      console.log(connection)
      // //console.log(`blasting ${connection.connectionId} with `, data)
      // await client.postToConnection({
      //   ConnectionId: connection.connectionId,
      //   Data: JSON.stringify(data)
      // })
      // .promise();
    }
  }

}