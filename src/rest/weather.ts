import WeatherLookup from '../WeatherLookup'

export const get = async (event: any = {}, context: any = {}): Promise<any> => {
  const data = event.queryStringParameters || JSON.parse(event.body)
  const weather = await WeatherLookup.getWeather(data.city)
  let response = {
    statusCode: 200,
    body: JSON.stringify(weather),
  }
  return response
}

export const list = async (event: any = {}, context: any = {}): Promise<any> => {
  const cities = await WeatherLookup.getCities()
  for (const city of cities) {
    city.current = await WeatherLookup.getCurrentConditions(city.slug)
  }
  let response = {
    statusCode: 200,
    body: JSON.stringify(cities),
  }
  return response
}