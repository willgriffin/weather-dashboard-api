## Install

```

# install the modules
npm install

# install dynamodb offline
./node_modules/.bin/serverless dynamodb install

# start the app
npm run serve

```